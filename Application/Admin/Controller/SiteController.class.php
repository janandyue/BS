<?php

namespace Admin\Controller;

use Common\Controller\AdminBaseController;

/**
 * 环境空气控制器
 */
class SiteController extends AdminBaseController {
    /**
     * 首页
     */
    public function index() {
        $siteList = getSite();
        $this->assign('siteList', $siteList);
        $this->display();

    }

    public function add() {
        if ($_POST) {
            $newSiteArray = array();
            $newSiteArray['site_name'] = $_POST['name'];
            $newSiteArray['is_use'] = $_POST['is_use'];
            $newSiteArray['addtime'] = time();
            $site = M("site");
            $res = $site->add($newSiteArray);
//            http://120.92.173.64/AirCollectionApi/api/Site
            $rDate = array();
            $rDate['siteNumber'] = "site1809-001";
            $rDate['siteName'] = trim($_POST['name']);
            $rDate['type'] = 1;
            $result = post_request('http://120.92.173.64/AirCollectionApi/api/Site',json_encode(json_decode(json_encode($rDate),true)));
            p($result);
            if ($res) {
                $this->success('站点添加成功', U('Admin/Site/index'));

            }
            else {
                $this->success('站点添加失败，请联系管理员', U('Admin/Site/index'));
            }


        }
        else {
            $this->success('非法操作', U('Admin/Site/index'));
        }
    }

    public function edit() {
        if ($_POST) {
            $id = $_POST['id'];
            $modifyArray['id'] = $id;
            $modifyArray['site_name'] = trim($_POST['site_name']);
            $modifyArray['is_use'] = intval($_POST['is_use']);
            $res = M("site")->where(array('id' => $id))->save($modifyArray);
            if($res){
                $this->success('成功修改站点', U('Admin/Site/index'));
            }else{
                $this->success('修改失败，请联系管理员', U('Admin/Site/index'));

            }

        }
        else {
            $this->success('非法操作', U('Admin/Site/index'));

        }
    }

    public function del() {
        $id = intval($_GET['id']);
        if ($id) {
            $siteInfo = M("site")->where(array('id' => $id))->find();
            $modifyArray = array();
            $modifyArray['is_use'] = 3;
            $modifyArray['id'] = $siteInfo['id'];
            $res = M("site")->where(array('id' => $id))->save($modifyArray);
            if ($res) {
                $this->success('成功删除站点', U('Admin/Site/index'));
            }
            else {
                $this->success('删除失败，请联系管理员', U('Admin/Site/index'));
            }

        }
        else {
            $this->success('非法操作', U('Admin/Site/index'));

        }
    }
    public function change() {
        $id = intval($_GET['id']);
        if ($id) {
            $siteInfo = M("site")->where(array('id' => $id))->find();
            $modifyArray = array();
            $modifyArray['is_use'] = $siteInfo['is_use']==1?2:1;
            $modifyArray['id'] = $siteInfo['id'];
            $res = M("site")->where(array('id' => $id))->save($modifyArray);
            if ($res) {
                $this->success('操作成功', U('Admin/Site/index'));
            }
            else {
                $this->success('操作失败', U('Admin/Site/index'));
            }

        }
        else {
            $this->success('非法操作', U('Admin/Site/index'));

        }
    }

    public function SiteInfo() {
        $id = intval($_GET['id']);
        $siteInfo = M("site")->where(array('id' => $id))->find();
        echo json_encode($siteInfo);
        exit;
    }

    /**
     * elements
     */
    public function elements() {

        $this->display();
    }

    /**
     * welcome
     */
    public function welcome() {
        $this->display();
    }

    public function getTimeSoloTypeOne($cha) {
        $yu = $cha % 3600;

        $final = $yu / 60;

        if ($final <= 5) {
            $fit = '05';
        }
        elseif ($final > 5 && $final <= 10) {
            $fit = '10';
        }
        elseif ($final > 10 && $final <= 15) {
            $fit = '15';
        }
        elseif ($final > 15 && $final <= 20) {
            $fit = '20';
        }
        elseif ($final > 20 && $final <= 25) {
            $fit = '25';
        }
        elseif ($final > 25 && $final <= 30) {
            $fit = '30';
        }
        elseif ($final > 30 && $final <= 35) {
            $fit = '35';
        }
        elseif ($final > 35 && $final <= 40) {
            $fit = '40';
        }
        elseif ($final > 40 && $final <= 45) {
            $fit = '45';
        }
        elseif ($final > 45 && $final <= 50) {
            $fit = '50';
        }
        elseif ($final > 50 && $final <= 55) {
            $fit = '55';
        }
        elseif ($final > 55 && $final <= 60) {
            $fit = '00';
        }
        return $fit;
    }

    public function getTimeSoloTypetwo($cha) {
        $fit = intval($cha / 3600);
        if ($fit < 10) {
            $fit = $fit . "0";
        }
        return $fit;
    }

    public function sortItem($timeType, $array, $name = null, $dayType = 1) {
        $newArray = array();
        $finalArray = array();
        if ($timeType == 1) {
            foreach ($array as $data) {
                if ($name == "NO") {
                    $min = $this->inMin(strtotime($data['adddate']) - strtotime(date("Y-m-d", strtotime($data['adddate']))), $dayType, strtotime(date("Y-m-d", strtotime($data['adddate']))));
                    $newArray[$min[0]][$min[1]][] = abs($data['airvalue']);
                }
                else {
                    $min = $this->inMin(strtotime($data['adddate']) - strtotime(date("Y-m-d", strtotime($data['adddate']))), $dayType, strtotime(date("Y-m-d", strtotime($data['adddate']))));
                    $newArray[$min[0]][$min[1]][] = $data['airvalue'];


                }
            }
            $total = array();
            foreach ($newArray as $k1 => $n) {
                foreach ($n as $k2 => $ttt) {
                    foreach ($ttt as $k3 => $t) {
                        $total[$k1][$k2] += $t;
                    }
                    $finalArray[$k1][$k2] = $total[$k1][$k2] / count($ttt);

                }

            }


        }
        else {
            foreach ($array as $data) {
                if ($name == "NO") {
                    $min = $this->inHour(strtotime($data['adddate']) - strtotime(date("Y-m-d", strtotime($data['adddate']))), $dayType, strtotime(date("Y-m-d", strtotime($data['adddate']))));
                    $newArray[$min[0]][$min[1]][] = abs($data['airvalue']);

                }
                else {
                    $min = $this->inHour(strtotime($data['adddate']) - strtotime(date("Y-m-d", strtotime($data['adddate']))), $dayType, strtotime(date("Y-m-d", strtotime($data['adddate']))));
                    $newArray[$min[0]][$min[1]][] = $data['airvalue'];

                }
            }
            $total = array();
            foreach ($newArray as $k1 => $n) {
                foreach ($n as $k2 => $ttt) {
                    foreach ($ttt as $k3 => $t) {
                        $total[$k1][$k2] += $t;
                    }
                    $finalArray[$k1][$k2] = $total[$k1][$k2] / count($ttt);

                }

            }


        }
        return $finalArray;
    }

    public function inMin($stampTime, $dayType, $date) {
        $day = date("Y-m-d", $date);
        $r = intval($stampTime / 300);
        return array($day, $r);
    }

    public function inHour($stampTime, $dayType, $date) {
        $day = date("Y-m-d", $date);
        $r = intval($stampTime / 3600);
        return array($day, $r);
    }

    public function getMAXLongItem() {

    }


}
