<?php

namespace Admin\Controller;

use Common\Controller\AdminBaseController;


/**
 * 环境空气控制器
 */
class AirController extends AdminBaseController {
    /**
     * 首页
     */
    public function index() {
        Vendor('PHPExcel.PHPExcel', '', '.php');
        $categories = array();

        $dayType = isset($_GET['dayType']) ? $_GET['dayType'] : 1;
        $day = isset($_GET['day']) ? $_GET['day'] : date("Y-m-d", time());
        $endday = isset($_GET['endday']) ? $_GET['endday'] : date("Y-m-d", time());
        $timeType = isset($_GET['timeType']) ? $_GET['timeType'] : 2;
        if ($dayType == 2) {
            $timeType = 2;
        }

        $chartName = '';
        $beagan = strtotime("2018-09-12 14:00:00");
        $order = "addtime desc";
        $limit = 35;
        $pre = 60;
        $showType = isset($_GET['showType']) ? $_GET['showType'] : 1;
        $showCO = isset($_GET['CO']) ? $_GET['CO'] : 0;
        $showNO = isset($_GET['NO']) ? $_GET['NO'] : 0;
        $showNO2 = isset($_GET['NO2']) ? $_GET['NO2'] : 0;
        $showNOX = isset($_GET['NOX']) ? $_GET['NOX'] : 0;
        $showO3 = isset($_GET['O3']) ? $_GET['O3'] : 0;
        $showPM10 = isset($_GET['PM10']) ? $_GET['PM10'] : 0;
        $showPM25 = isset($_GET['PM25']) ? $_GET['PM25'] : 0;
        $showSO2 = isset($_GET['SO2']) ? $_GET['SO2'] : 0;
        $siteNumber = isset($_GET['siteNumber']) ? $_GET['siteNumber'] : 'site1809-001';
        if ($showCO == 0 && $showNO == 0 && $showNO2 == 0 && $showNOX == 0 && $showO3 == 0 && $showPM10 == 0 && $showPM25 == 0 && $showSO2 == 0) {
            $showCO = 1;
            $showNO = 1;
            $showNO2 = 1;
            $showNOX = 1;
            $showO3 = 1;
            $showPM10 = 1;
            $showPM25 = 1;
            $showSO2 = 1;;
        }
        if ($timeType == 1) {
            //五分钟
            $pre = 10;
            $order = 'adddate asc';
            $limit = 100000;
            $tt = +300;

        }
        else {
            $pre = 120;
            $order = 'adddate asc';
            $limit = 100000;
            $tt = +3600;

        }
        if ($dayType == 1) {
            $beagan = date("Y-m-d H:i:s", strtotime($day));
            $end = date("Y-m-d H:i:s", strtotime($day) + 86400);
            $where = "adddate BETWEEN '{$beagan}'  and '{$end}' ";
        }
        else {
            $beagan = date("Y-m-d H:i:s", strtotime($day));
            $end = date("Y-m-d H:i:s", strtotime($endday) + 86400);
            $where = "adddate BETWEEN '{$beagan}'  and '{$end}' ";

        }
        $sbegan = strtotime($beagan);
        $send = strtotime($endday);
        $monthArray = array();
        for ($i = $sbegan; $i <= $send; $i = $i + 86400) {
            $monthArray[date("Y-m", $i)] = 'a';
        }
        //
        if (count($monthArray) != 1&&$dayType==2) {
            for ($i = $sbegan; $i <= $send; $i = $i + 86400) {
                                $rDate = array();
                                $rDate['siteNumber'] = $siteNumber;
                                $rDate['sDate'] = date("Y-m-d H:i:s",$i);
                                $rDate['eDate'] = date("Y-m-d H:i:s",$i+86400);
                                $rDate['type'] = intval($timeType);
                                $airtempDate[] = CurlGet('http://120.92.173.64/AirCollectionApi/api/AirData', $rDate);
            }
            foreach ($airtempDate as $temp){
                foreach ($temp as $t){
                    $airData[] = $t;
                }
            }
        }
        else {
            //不垮月
            $rDate = array();
            $rDate['siteNumber'] = $siteNumber;
            $rDate['sDate'] = $beagan;
            $rDate['eDate'] = $end;
            $rDate['type'] = intval($timeType);
            $airData = CurlGet('http://120.92.173.64/AirCollectionApi/api/AirData', $rDate);
        }
        foreach ($airData as $k => $air) {
            $airData[$k]['strTime'] = str_replace("T", " ", $air['strTime']);
            $airData[$k]['strTime2'] = date("H:i:s",strtotime($air['strTime']));
            if ($airData[$k]['CO'] != 0) {
                $airData[$k]['CO'] = valueFamat("%.2f", $air['CO'] / 1000);
            }
            if ($airData[$k]['PM10'] < 0) {
                $airData[$k]['PM10'] = 0.01;
            }
            if ($timeType == 1) {
                $categories[] = date("H:i", strtotime($airData[$k]['strTime']));
            }
            else {
                $categories[] = date("H:00", strtotime($airData[$k]['strTime']));
            }

        }
        $categoriesData[] = SortDataToChart(getArrayFields($airData, 'CO'), '一氧化碳CO');
        $categoriesData[] = SortDataToChart(getArrayFields($airData, 'NO'), '一氧化氮NO');
        $categoriesData[] = SortDataToChart(getArrayFields($airData, 'NO2'), '二氧化氮NO₂');
        $categoriesData[] = SortDataToChart(getArrayFields($airData, 'NOX'), '氮氧化物NOX');
        $categoriesData[] = SortDataToChart(getArrayFields($airData, 'O3'), '臭氧O₃');
        $categoriesData[] = SortDataToChart(getArrayFields($airData, 'PM10'), 'Pm10');
        $categoriesData[] = SortDataToChart(getArrayFields($airData, 'PM25'), 'Pm2.5');
        $categoriesData[] = SortDataToChart(getArrayFields($airData, 'SO2'), '二氧化硫SO₂');
        $siteList = getSite();
        //趋势图X轴
        $assign = array('airData' => $airData, 'timeType' => $timeType, 'hourList' => getDaylist(), 'categories' => json_encode($categories), 'categoriesData' => json_encode($categoriesData));
        $this->assign($assign);
        $this->assign('categories', json_encode($categories));
        $this->assign('categoriesData', json_encode($categoriesData));
        $this->assign('timeType', $timeType);
        $this->assign('dayType', $dayType);
        $this->assign('siteList', $siteList);

        $this->assign('day', date("Y-m-d", strtotime($day)));
        $this->assign('endday', date("Y-m-d", strtotime($endday)));
        $this->assign('endDate', date("Y-m-d", time()));
        $this->assign('chartName', $chartName);
        $this->assign('showCO', $showCO);
        $this->assign('showNO2', $showNO2);
        $this->assign('showNO', $showNO);
        $this->assign('showNOX', $showNOX);
        $this->assign('showO3', $showO3);
        $this->assign('showPM10', $showPM10);
        $this->assign('showPM25', $showPM25);
        $this->assign('showSO2', $showSO2);
        $this->assign('siteNumber', $siteNumber);
        if ($_GET['json'] == 1) {
            if ($_GET['type'] == 1) {
                //                import('Vendor.phpoffice.phpspreadsheet');
                //                use PhpOffice\PhpSpreadsheet\Helper\Sample;
                //                use PhpOffice\PhpSpreadsheet\IOFactory;
                //                use PhpOffice\PhpSpreadsheet\Spreadsheet;
                $timeName = $timeType == 1 ? "5分钟" : "小时";
                $name = getSiteName($siteNumber)." ".date("Y-m-d", strtotime($beagan)) . "-" . date("Y-m-d", strtotime($end)) . "（每" . $timeName . "）.xls";
                vendor('PHPExcel.PHPExcel.php');
                $excelArray = array();
                $excelArray[0]['strTime'] = "时间";
                $excelArray[0]['SO2'] = "二氧化硫SO₂(ug/m³)";
                $excelArray[0]['NO'] = "一氧化氮NO(ug/m³)";
                $excelArray[0]['NO2'] = "二氧化氮NO₂(ug/m³)";
                $excelArray[0]['NOX'] = "氮氧化物NOX(ug/m³)";
                $excelArray[0]['CO'] = "一氧化碳CO(mg/m³)";
                $excelArray[0]['O3'] = "臭氧O₃(ug/m³)";
                $excelArray[0]['PM10'] = "PM10(ug/m³)";
                $excelArray[0]['PM25'] = "PM2.5(ug/m³)";

                $excelArray[1]['strTime'] = "最大值";
                $excelArray[1]['SO2'] = getArrayMax($airData, 'SO2');
                $excelArray[1]['NO'] = getArrayMax($airData, 'NO');
                $excelArray[1]['NO2'] = getArrayMax($airData, 'NO2');
                $excelArray[1]['NOX'] = getArrayMax($airData, 'NOX');
                $excelArray[1]['CO'] = getArrayMax($airData, 'CO');
                $excelArray[1]['O3'] = getArrayMax($airData, 'O3');
                $excelArray[1]['PM10'] = getArrayMax($airData, 'PM10');
                $excelArray[1]['PM25'] = getArrayMax($airData, 'PM25');

                $excelArray[2]['strTime'] = "最小值";
                $excelArray[2]['SO2'] = getArrayLow($airData, 'SO2');
                $excelArray[2]['NO'] = getArrayLow($airData, 'NO');
                $excelArray[2]['NO2'] = getArrayLow($airData, 'NO2');
                $excelArray[2]['NOX'] = getArrayLow($airData, 'NOX');
                $excelArray[2]['CO'] = getArrayLow($airData, 'CO');
                $excelArray[2]['O3'] = getArrayLow($airData, 'O3');
                $excelArray[2]['PM10'] = getArrayLow($airData, 'PM10');
                $excelArray[2]['PM25'] = getArrayLow($airData, 'PM25');

                $excelArray[3]['strTime'] = "最小值";
                $excelArray[3]['SO2'] = getArrayAvg($airData, 'SO2');
                $excelArray[3]['NO'] = getArrayAvg($airData, 'NO');
                $excelArray[3]['NO2'] = getArrayAvg($airData, 'NO2');
                $excelArray[3]['NOX'] = getArrayAvg($airData, 'NOX');
                $excelArray[3]['CO'] = getArrayAvg($airData, 'CO');
                $excelArray[3]['O3'] = getArrayAvg($airData, 'O3');
                $excelArray[3]['PM10'] = getArrayAvg($airData, 'PM10');
                $excelArray[3]['PM25'] = getArrayAvg($airData, 'PM25');
                foreach ($airData as $data) {
                    unset($data['strTime2']);
                    $excelArray[] = $data;
                }
                create_xls($excelArray, $name);


            }
            else {
                p();
            }
            echo json_encode($data);
            exit;

        }
        else {
            $this->display();

        }
    }

    public function History() {

    }

    /**
     * elements
     */
    public function elements() {

        $this->display();
    }

    /**
     * welcome
     */
    public function welcome() {
        $this->display();
    }

    public function getTimeSoloTypeOne($cha) {
        $yu = $cha % 3600;

        $final = $yu / 60;

        if ($final <= 5) {
            $fit = '05';
        }
        elseif ($final > 5 && $final <= 10) {
            $fit = '10';
        }
        elseif ($final > 10 && $final <= 15) {
            $fit = '15';
        }
        elseif ($final > 15 && $final <= 20) {
            $fit = '20';
        }
        elseif ($final > 20 && $final <= 25) {
            $fit = '25';
        }
        elseif ($final > 25 && $final <= 30) {
            $fit = '30';
        }
        elseif ($final > 30 && $final <= 35) {
            $fit = '35';
        }
        elseif ($final > 35 && $final <= 40) {
            $fit = '40';
        }
        elseif ($final > 40 && $final <= 45) {
            $fit = '45';
        }
        elseif ($final > 45 && $final <= 50) {
            $fit = '50';
        }
        elseif ($final > 50 && $final <= 55) {
            $fit = '55';
        }
        elseif ($final > 55 && $final <= 60) {
            $fit = '00';
        }
        return $fit;
    }

    public function getTimeSoloTypetwo($cha) {
        $fit = intval($cha / 3600);
        if ($fit < 10) {
            $fit = $fit . "0";
        }
        return $fit;
    }

    public function sortItem($timeType, $array, $name = null, $dayType = 1) {
        $newArray = array();
        $finalArray = array();
        if ($timeType == 1) {
            foreach ($array as $data) {
                if ($name == "NO") {
                    $min = $this->inMin(strtotime($data['adddate']) - strtotime(date("Y-m-d", strtotime($data['adddate']))), $dayType, strtotime(date("Y-m-d", strtotime($data['adddate']))));
                    $newArray[$min[0]][$min[1]][] = abs($data['airvalue']);
                }
                else {
                    $min = $this->inMin(strtotime($data['adddate']) - strtotime(date("Y-m-d", strtotime($data['adddate']))), $dayType, strtotime(date("Y-m-d", strtotime($data['adddate']))));
                    $newArray[$min[0]][$min[1]][] = $data['airvalue'];


                }
            }
            $total = array();
            foreach ($newArray as $k1 => $n) {
                foreach ($n as $k2 => $ttt) {
                    foreach ($ttt as $k3 => $t) {
                        $total[$k1][$k2] += $t;
                    }
                    $finalArray[$k1][$k2] = $total[$k1][$k2] / count($ttt);

                }

            }


        }
        else {
            foreach ($array as $data) {
                if ($name == "NO") {
                    $min = $this->inHour(strtotime($data['adddate']) - strtotime(date("Y-m-d", strtotime($data['adddate']))), $dayType, strtotime(date("Y-m-d", strtotime($data['adddate']))));
                    $newArray[$min[0]][$min[1]][] = abs($data['airvalue']);

                }
                else {
                    $min = $this->inHour(strtotime($data['adddate']) - strtotime(date("Y-m-d", strtotime($data['adddate']))), $dayType, strtotime(date("Y-m-d", strtotime($data['adddate']))));
                    $newArray[$min[0]][$min[1]][] = $data['airvalue'];

                }
            }
            $total = array();
            foreach ($newArray as $k1 => $n) {
                foreach ($n as $k2 => $ttt) {
                    foreach ($ttt as $k3 => $t) {
                        $total[$k1][$k2] += $t;
                    }
                    $finalArray[$k1][$k2] = $total[$k1][$k2] / count($ttt);

                }

            }


        }
        return $finalArray;
    }

    public function inMin($stampTime, $dayType, $date) {
        $day = date("Y-m-d", $date);
        $r = intval($stampTime / 300);
        return array($day, $r);
    }

    public function inHour($stampTime, $dayType, $date) {
        $day = date("Y-m-d", $date);
        $r = intval($stampTime / 3600);
        return array($day, $r);
    }

    public function getMAXLongItem() {

    }

    private function _curlCall($method, $url, $rest_data, $timeout = 20) {
        $rest_data = json_decode($rest_data, 1);
        $rest_data['sign'] = strtoupper($rest_data['sign']);
        $rest_data = json_encode($rest_data);
        try {
            //curl 调用
            $curl = curl_init();
            $this_header = array("content-type: application/json; charset=utf-8");

            curl_setopt($curl, CURLOPT_POSTFIELDS, $rest_data);
            //ttt($rest_data);
            //curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($curl, CURLOPT_URL, $url);
            $timeout = intval($timeout) ? intval($timeout) : 20;
            curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this_header);
            $result = curl_exec($curl);
            print_r($result);
            $curl_err_code = curl_errno($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

        } catch (exception $e) {
            //抛出异常
            throw $e;
        }
        return $result;
    }

    private function curlhelper($url, $data, $ssl = 0) {
        $ch = curl_init();
        $timeout = 30;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: multipart/form-data"));
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); // 设置超时限制防止死循环
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        if ($ssl == 0) {
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        else if ($ssl == 1) {
            $data = http_build_query($data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
            curl_setopt($ch, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        }

        $tmpInfo = curl_exec($ch); // 执行操作
        if (curl_errno($ch)) {
            return 'err_' . curl_error($ch);//捕抓异常
        }

        curl_close($ch); // 关闭CURL会话
        return $tmpInfo;
    }

    private function request($url) {
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: multipart/form-data"));

        curl_setopt($curl, CURLOPT_URL, $url);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 1);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //执行命令
        $data = curl_exec($curl);
        //关闭URL请求
        curl_close($curl);
        //显示获得的数据
        return $data;
    }

    private function curl_post($url = '', $param = '') {
        if (empty($url) || empty($param)) {
            return false;
        }
        $postUrl = $url;
        $curlPost = $param;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    private function testAction() {
        $url = '';
        $post_data['appKey'] = '';
        $post_data['regionId'] = '';
        $post_data['token'] = '';
        $post_data['sign'] = '';

        $o = "";
        foreach ($post_data as $k => $v) {
            $o .= "$k=" . urlencode($v) . "&";
        }
        $post_data = substr($o, 0, -1);
        $res = $this->curl_post($url, $post_data);
        print_r($res);

    }


}
