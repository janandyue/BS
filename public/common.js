var ServerDomain = "http://" + window.location.host + "/"

function AjaxPost(url, postData) {
    url = ServerDomain + url
    var result
    $.ajax({
        type: "POST",
        url: url,
        data: postData,
        async: false,
        dataType: "json",
        success: function (data) {
            result = data
        },
    })
    return result
}

function AjaxPostAsync(url, postData, callback) {
    console.dir(postData)
    url = ServerDomain + url
    $.ajax({
        type: "POST",
        url: url,
        data: postData,
        async: true,
        dataType: "json",
        success: function (data) {
            callback(data)
        },
    })
}

function AjaxGet(url, postData) {
    url = ServerDomain + "" + url
    // url = ServerDomain+"/suit" + url
    var result
    $.ajax({
        type: "get",
        url: url,
        data: postData,
        async: false,
        dataType: "json",
        success: function (data) {
            p(data)
            result = data
        },
    })
    return result
}

function AjaxGetAsync(url, getData) {
    url = ServerDomain + url
    var result
    $.ajax({
        type: "get",
        url: url,
        data: getData,
        async: true,
        dataType: "json",
        success: function (data) {
            result = data
        },
    })
    return result
}

function setHeight(el) {
    var screen = window.screen.height + "px"
    document.querySelector(el).style.height = screen
    return true
}

function setHeightExtra(el, extra) {
    var screen = window.screen.height + extra + "px"
    document.querySelector(el).style.height = screen
    console.dir(document.querySelector(el));

    return true
}