<?php
/**
 * Created by PhpStorm.
 * User: jan
 * Date: 2018/9/29
 * Time: 2:44 PM
 */


require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$siteList = CurlGet('http://120.92.173.64/AirCollectionApi/api/Site', array());

$dayType = isset($_GET['dayType']) ? $_GET['dayType'] : 1;
$day = isset($_GET['day']) ? $_GET['day'] : date("Y-m-d", time());
$endday = isset($_GET['endday']) ? $_GET['endday'] : date("Y-m-d", time());
$timeType = isset($_GET['timeType']) ? $_GET['timeType'] : 1;
if ($dayType == 2) {
    $timeType = 2;
}

$chartName = '';
$beagan = strtotime("2018-09-12 14:00:00");
$order = "addtime desc";
$limit = 35;
$pre = 60;
$showType = isset($_GET['showType']) ? $_GET['showType'] : 1;
$showCO = isset($_GET['CO']) ? $_GET['CO'] : 0;
$showNO = isset($_GET['NO']) ? $_GET['NO'] : 0;
$showNO2 = isset($_GET['NO2']) ? $_GET['NO2'] : 0;
$showNOX = isset($_GET['NOX']) ? $_GET['NOX'] : 0;
$showO3 = isset($_GET['O3']) ? $_GET['O3'] : 0;
$showPM10 = isset($_GET['PM10']) ? $_GET['PM10'] : 0;
$showPM25 = isset($_GET['PM25']) ? $_GET['PM25'] : 0;
$showSO2 = isset($_GET['SO2']) ? $_GET['SO2'] : 0;
if ($showCO == 0 && $showNO == 0 && $showNO2 == 0 && $showNOX == 0 && $showO3 == 0 && $showPM10 == 0 && $showPM25 == 0 && $showSO2 == 0) {
    $showCO = 1;
    $showNO = 1;
    $showNO2 = 1;
    $showNOX = 1;
    $showO3 = 1;
    $showPM10 = 1;
    $showPM25 = 1;
    $showSO2 = 1;;
}
if ($timeType == 1) {
    //五分钟
    $pre = 10;
    $order = 'adddate asc';
    $limit = 100000;
    $tt = +300;

}
else {
    $pre = 120;
    $order = 'adddate asc';
    $limit = 100000;
    $tt = +3600;

}
if ($dayType == 1) {
    $beagan = date("Y-m-d H:i:s", strtotime($day));
    $end = date("Y-m-d H:i:s", strtotime($day) + 86400);
    $where = "adddate BETWEEN '{$beagan}'  and '{$end}' ";
}
else {
    $beagan = date("Y-m-d H:i:s", strtotime($day));
    $end = date("Y-m-d H:i:s", strtotime($endday) + 86400);
    $where = "adddate BETWEEN '{$beagan}'  and '{$end}' ";

}
$rDate = array();
$rDate['siteNumber'] = 'site1809-001';
$rDate['sDate'] = $beagan;
$rDate['eDate'] = $end;
$rDate['type'] = intval($timeType);
$airData = CurlGet('http://120.92.173.64/AirCollectionApi/api/AirData', $rDate);

if ($_GET['type'] == 1) {
    $excelArray = $airData;
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', '时间');
    $sheet->setCellValue('B1', '二氧化硫SO₂(ug/m³)');
    $sheet->setCellValue('C1', '一氧化氮NO(ug/m³)');
    $sheet->setCellValue('D1', '二氧化氮NO₂(ug/m³)');
    $sheet->setCellValue('E1', '氮氧化物NOX(ug/m³)');
    $sheet->setCellValue('F1', '一氧化碳CO(mg/m³)');
    $sheet->setCellValue('G1', '臭氧O₃(ug/m³');
    $sheet->setCellValue('H1', 'PM10(ug/m³)');
    $sheet->setCellValue('I1', 'PM2.5(ug/m³)');
    $spreadsheet->getActiveSheet()->fromArray($excelArray, NULL, 'A2');
    $writer = new Xlsx($spreadsheet);
    $fileName = $_SERVER['DOCUMENT_ROOT'] . '/file/' . date('Y-m-d') . '.xlsx';
    $writer->save($fileName);
    $data['filename'] = date('Y-m-d') . '.xlsx';
}else{

}
echo json_encode($data);



function CurlGet($url, $params) {
    $url = $url . "?" . http_build_query($params);
    $ch = curl_init();
    $this_header = array();
    $this_header[] = "Content-Type: application/x-www-form-urlencoded";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header);
    //        curl_setopt($ch, CURLOPT_POST, 2);                      // 不需要页面的HTTP头
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    //        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);//如果不加验证,就设false,商户自行处理
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, 0);                      // 不需要页面的HTTP头
    $output = curl_exec($ch);
    curl_close($ch);
    return json_decode(json_decode($output, true), true);
}