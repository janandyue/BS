<?php
/**
 * 登陆页面
 * Created by PhpStorm.
 * User: jan
 * Date: 2018/8/20
 * Time: 12:46 PM
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/function/core.php';
if (checkLogin() == true) {
    JUMP('admin/dashboard.php');
}
?>

<!DOCTYPE html>
<html lang="Zh">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="public/BS.css">
    <link rel="stylesheet" href="public/login.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="<?php echo $config['jquery'] ?>"></script>
    <script src="public/common.js"></script>
    <title><?php echo $config['site_name']; ?> 登陆</title>
</head>
<style>
    #id-login-content {
        position: fixed;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        width: 350px;
        height: 240px;
        background-color: white;
        border-radius: 5px;
        padding: 0 40px 40px 40px;
    }

    #id-login-content .content {
        width: 100%;
        min-height: 35px;
    }

    #id-login-content .content input[type=text], #id-login-content .content input[type=password] {
        border: 1px solid #D5D5D5;
        width: 338px;
        height: 30px;
        padding-left: 5px;
        padding-right: 5px;
        line-height: 20px;
        font-size: 14px;
    }

    #id-login-content .content button {
        width: 280px;
        height: 40px;
        border-color: #2196F3;
        background: #2196F3;
        color: white;
        line-height: 25px;
        font-size: 16px;

    }

    #id-login-content .bb1 {
        border-bottom: 1px solid #ddd;
    }

    #id-login-content .mt1 {
        margin-top: 10px;
    }

    #id-login-content .ma {
        margin: 0 auto;
    }

    #id-login-content .tac {
        text-align: center;
    }

    #id-login-content .mt2 {
        margin-top: 20px;
    }
</style>
<body>

<div id="id-login-content">

    <div class="content" style="margin: 20px 0 ">
        <h1 style="font-size: 22px;font-weight: bold;text-align: center">
            中天钢铁环境空气质量监测平台
        </h1>
    </div>
    <div class="content bb1">
        <h1><i class="fa fa-coffee"></i>&nbsp;输入您的账号和密码</h1>
    </div>
    <div class="content mt1">
        <input type="text" name="username" placeholder="用户名">
    </div>
    <div class="content mt1">
        <input type="password" name="password" placeholder="密码">
    </div>
    <div class="content   tac  " style="margin-top: 20px">
        <button onclick="login()">登陆</button>
    </div>
</div>
</body>
<script>
    function login() {
        var username = $('input[name=username]').val()
        var password = $('input[name=password]').val()
        var data = AjaxPost('ajax/index.php', {
            method: 'login',
            username: username,
            password: password,
        })
        if (data.status == true) {
            window.location.href = '/admin/dashboard.php'
        } else {
            alert(data.message)
        }
    }

    $(function () {


    })
</script>
</html>
