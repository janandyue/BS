<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
         中天钢铁环境空气质量监测平台
    </title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="/public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/public/statics/aceadmin/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/public/statics/aceadmin/css/font-awesome-ie7.min.css" /><![endif]-->
    <link rel="stylesheet" href="/public/statics/aceadmin/css/ace.min.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/public/statics/aceadmin/css/ace-ie.min.css" /><![endif]--><!--[if lt IE 9]>
    <script src="/public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <link rel="stylesheet" href="/tpl/Public/css/base.css" />
    
    <style type="text/css">
        #sidebar .nav-list {
            overflow-y: auto;
        }
        
        .b-nav-li {
            padding: 5px 0;
        }
    </style>
    
</head>
<body>
<div class="navbar navbar-default" id="navbar">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left"><a href="#" class="navbar-brand">
            <small><i class="icon-leaf"></i> 中天钢铁环境空气质量监测平台</small>
        </a></div>
        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle"><img class="nav-user-photo" src="/public/statics/aceadmin/avatars/user.jpg" alt="Jason's Photo" />
                        <span class="user-info"><small>欢迎光临,</small> <?php echo ($_SESSION['user']['username']); ?></span><i class="icon-caret-down"></i></a>
                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li class="divider"></li>
                        <li><a href="<?php echo U('Home/Index/logout');?>"><i class="icon-off"></i> 退出</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="main-container-inner"><a class="menu-toggler" id="menu-toggler" href="#"><span class="menu-text"></span></a>
        <div class="sidebar" id="sidebar">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                    <button class="btn btn-success"><i class="icon-signal"></i></button>
                    <button class="btn btn-info"><i class="icon-pencil"></i></button>
                    <button class="btn btn-warning"><i class="icon-group"></i></button>
                    <button class="btn btn-danger"><i class="icon-cogs"></i></button>
                </div>
                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span><span class="btn btn-info"></span><span class="btn btn-warning"></span><span class="btn btn-danger"></span>
                </div>
            </div><!-- #sidebar-shortcuts -->
            <ul class="nav nav-list">
                <?php if(is_array($nav_data)): foreach($nav_data as $key=>$v): if(empty($v['_data'])): ?><li class="b-nav-li"><a href="<?php echo U($v['mca']);?>"><i class="fa fa-<?php echo ($v['ico']); ?> icon-test"></i>
                            <span class="menu-text"><?php echo ($v['name']); ?></span></a></li>
                        <?php else: ?>
                        <li class="b-has-child">
                            <a href="#" class="dropdown-toggle b-nav-parent"><i class="fa fa-<?php echo ($v['ico']); ?> icon-test"></i>
                                <span class="menu-text"><?php echo ($v['name']); ?></span><b class="arrow icon-angle-down"></b></a>
                            <ul class="submenu">
                                <?php if(is_array($v['_data'])): foreach($v['_data'] as $key=>$n): ?><li class="b-nav-li <?php if( (MODULE_NAME. '/'.CONTROLLER_NAME.'/'.ACTION_NAME) == $n['mca']): ?>active<?php endif; ?>"><a href="<?php echo U($n['mca']);?>"><i class="icon-double-angle-right"></i> <?php echo ($n['name']); ?></a></li><?php endforeach; endif; ?>
                            </ul>
                        </li><?php endif; endforeach; endif; ?>
            </ul>
            <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
            </div>
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {
                }
            </script>
        </div>
        <div class="main-content">
            <div class="page-content">
                
    <div class="page-header"><h1><i class="fa fa-home"></i> 首页 &gt;空气环境</h1></div>
    <div class="col-xs-12">
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="clearfix">
                                <div class="pull-right tableTools-container"></div>
                            </div>
                            <div class="tabbable">
                                <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">
                                    <li class="active">
                                        <a data-toggle="tab" href="#home4" aria-expanded="true">数据</a>
                                    </li>
                                    <li class="">
                                        <a data-toggle="tab" href="#profile4" aria-expanded="false">趋势图</a>
                                    </li>
                                    <li class="">
                                        <a data-toggle="tab" href="#dropdown14" aria-expanded="false">玫瑰图</a>
                                    </li>
                                </ul>
                                
                                <div class="tab-content">
                                    <div id="home4" class="tab-pane active">
                                        <div class="table-header">
                                            <span>查询类型</span>
                                            <select name="queryType" id="queryType" onchange="changeQueryType()">
                                                <option value="1" <?php if($queryType == 1): ?>selected<?php endif; ?>>最新数据</option>
                                                <option value="2" <?php if($queryType == 2): ?>selected<?php endif; ?>>其他时间</option>
                                            </select>
                                            
                                            <span class="queryType1">&nbsp;&nbsp;&nbsp;&nbsp;数据区间</span>
                                            <select name="timeType" class="queryType1" id="timeType" onchange="changeTimeType()">
                                                <option value="1" <?php if($timeType == 1): ?>selected<?php endif; ?>>每5分钟</option>
                                                <option value="2" <?php if($timeType == 2): ?>selected<?php endif; ?>>每1小时</option>
                                            </select>
    
                                            <span class="queryType2">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <select name="showType" class="queryType2" id="oldQueryTime" onchange="changeOldQueryType()">
                                                <option value="1" <?php if($oldQueryTime == 1): ?>selected<?php endif; ?>> 按小时查询</option>
                                                <option value="2" <?php if($oldQueryTime == 2): ?>selected<?php endif; ?>>按日查询</option>
                                            </select>
                                            
                                            
                                            <span class="queryType2 oldQueryTime1">&nbsp;&nbsp;&nbsp;&nbsp;查询时间</span>
                                            <select name="queryType2" id="hour" class="queryType2 oldQueryTime1" id="">
                                                <?php if(is_array($hourList)): foreach($hourList as $key=>$v): ?><option <?php if($hour == $v['time']): ?>selected<?php endif; ?> value="<?php echo ($v['time']); ?>"><?php echo ($v['name']); ?></option><?php endforeach; endif; ?>
                                            </select>
                                            <span class="">
                                                <input style="width: 120px;" name="day" value="<?php echo ($day); ?>" class=" date-picker oldQueryTime2" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy">
                                                </span>
                                            <button class="btn btn-sm  btn-yellow" onclick="$('#selector').submit()">查询</button>
                                            <!--<button style="margin-top: 6px;margin-right: 5px" onclick="" class="btn btn-minier btn-yellow">查询</button>-->
                                            <span>刷新时间</span>
                                            <select name="showType">
                                                <option value="10">10秒刷新</option>
                                                <option value="60">60秒刷新</option>
                                                <option value="300">5分钟刷新</option>
                                            </select>
                                            <button style="float: right;margin-top: 6px;margin-right: 5px" onclick="window.location.reload()" class="btn btn-minier btn-yellow">刷新</button>
                                        </div>
                                        <div>
                                            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th>时间</th>
                                                    <th>一氧化碳CO(ug/m³)</th>
                                                    <th>一氧化氮NO(ug/m³)</th>
                                                    <th>二氧化氮CO₂(ug/m³)</th>
                                                    <th>氮氧化物NOX(ug/m³)</th>
                                                    <th>臭氧O₃(ug/m³)</th>
                                                    <th>PM10(ug/m³)</th>
                                                    <th>PM2.5(ug/m³)</th>
                                                    <th>二氧化硫SO₂(ug/m³)</th>
                                                </tr>
                                                </thead>
                                                
                                                <tbody>
                                                <?php if(is_array($airData)): foreach($airData as $key=>$v): ?><tr>
                                                        
                                                        <td><?php echo ($v['strTime']); ?></td>
                                                        <td><?php echo ($v['CO']); ?></td>
                                                        <td><?php echo ($v['NO']); ?></td>
                                                        <td><?php echo ($v['NO2']); ?></td>
                                                        <td><?php echo ($v['NOX']); ?></td>
                                                        <td><?php echo ($v['O3']); ?></td>
                                                        <td><?php echo ($v['NOX']); ?></td>
                                                        <td><?php echo ($v['PM25']); ?></td>
                                                        <td><?php echo ($v['SO2']); ?></td>
                                                    </tr><?php endforeach; endif; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                    <div id="profile4" class="tab-pane">
                                        <div id="qushitu" style="min-width:400px;height:400px"></div>
                                        
                                    </div>
                                    
                                    <div id="dropdown14" class="tab-pane">
                                        <div id="meigui" style=" height: 400px; margin: 0 auto"></div>
                                        <div style="display:none">
                                            <!-- Source: http://or.water.usgs.gov/cgi-bin/grapher/graph_windrose.pl -->
                                            <table id="freq" border="0" cellspacing="0" cellpadding="0">
                                                <tr nowrap bgcolor="#CCCCFF">
                                                    <th colspan="9" class="hdr">Table of Frequencies (percent)</th>
                                                </tr>
                                                <tr nowrap bgcolor="#CCCCFF">
                                                    <th class="freq">Direction</th>
                                                    <th class="freq">< 0.5 m/s</th>
                                                    <th class="freq">0.5-2 m/s</th>
                                                    <th class="freq">2-4 m/s</th>
                                                    <th class="freq">4-6 m/s</th>
                                                    <th class="freq">6-8 m/s</th>
                                                    <th class="freq">8-10 m/s</th>
                                                    <th class="freq">> 10 m/s</th>
                                                    <th class="freq">Total</th>
                                                </tr>
                                                <tr nowrap>
                                                    <td class="dir">N</td>
                                                    <td class="data">1.81</td>
                                                    <td class="data">1.78</td>
                                                    <td class="data">0.16</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">3.75</td>
                                                </tr>
                                                <tr nowrap bgcolor="#DDDDDD">
                                                    <td class="dir">NNE</td>
                                                    <td class="data">0.62</td>
                                                    <td class="data">1.09</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">1.71</td>
                                                </tr>
                                                
                                                <tr nowrap bgcolor="#DDDDDD">
                                                    <td class="dir">ENE</td>
                                                    <td class="data">0.59</td>
                                                    <td class="data">1.22</td>
                                                    <td class="data">0.07</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">1.88</td>
                                                </tr>
                                                <tr nowrap>
                                                    <td class="dir">E</td>
                                                    <td class="data">0.62</td>
                                                    <td class="data">2.20</td>
                                                    <td class="data">0.49</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">3.32</td>
                                                </tr>
                                                <tr nowrap bgcolor="#DDDDDD">
                                                    <td class="dir">ESE</td>
                                                    <td class="data">1.22</td>
                                                    <td class="data">2.01</td>
                                                    <td class="data">1.55</td>
                                                    <td class="data">0.30</td>
                                                    <td class="data">0.13</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">5.20</td>
                                                </tr>
                                                <tr nowrap>
                                                    <td class="dir">SE</td>
                                                    <td class="data">1.61</td>
                                                    <td class="data">3.06</td>
                                                    <td class="data">2.37</td>
                                                    <td class="data">2.14</td>
                                                    <td class="data">1.74</td>
                                                    <td class="data">0.39</td>
                                                    <td class="data">0.13</td>
                                                    <td class="data">11.45</td>
                                                </tr>
                                                <tr nowrap bgcolor="#DDDDDD">
                                                    <td class="dir">SSE</td>
                                                    <td class="data">2.04</td>
                                                    <td class="data">3.42</td>
                                                    <td class="data">1.97</td>
                                                    <td class="data">0.86</td>
                                                    <td class="data">0.53</td>
                                                    <td class="data">0.49</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">9.31</td>
                                                </tr>
                                                <tr nowrap>
                                                    <td class="dir">S</td>
                                                    <td class="data">2.66</td>
                                                    <td class="data">4.74</td>
                                                    <td class="data">0.43</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">7.83</td>
                                                </tr>
                                                <tr nowrap bgcolor="#DDDDDD">
                                                    <td class="dir">SSW</td>
                                                    <td class="data">2.96</td>
                                                    <td class="data">4.14</td>
                                                    <td class="data">0.26</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">7.37</td>
                                                </tr>
                                                <tr nowrap>
                                                    <td class="dir">SW</td>
                                                    <td class="data">2.53</td>
                                                    <td class="data">4.01</td>
                                                    <td class="data">1.22</td>
                                                    <td class="data">0.49</td>
                                                    <td class="data">0.13</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">8.39</td>
                                                </tr>
                                                <tr nowrap bgcolor="#DDDDDD">
                                                    <td class="dir">WSW</td>
                                                    <td class="data">1.97</td>
                                                    <td class="data">2.66</td>
                                                    <td class="data">1.97</td>
                                                    <td class="data">0.79</td>
                                                    <td class="data">0.30</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">7.70</td>
                                                </tr>
                                                <tr nowrap>
                                                    <td class="dir">W</td>
                                                    <td class="data">1.64</td>
                                                    <td class="data">1.71</td>
                                                    <td class="data">0.92</td>
                                                    <td class="data">1.45</td>
                                                    <td class="data">0.26</td>
                                                    <td class="data">0.10</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">6.09</td>
                                                </tr>
                                                <tr nowrap bgcolor="#DDDDDD">
                                                    <td class="dir">WNW</td>
                                                    <td class="data">1.32</td>
                                                    <td class="data">2.40</td>
                                                    <td class="data">0.99</td>
                                                    <td class="data">1.61</td>
                                                    <td class="data">0.33</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">0.00</td>
                                                    <td class="data">6.64</td>
                                                </tr>
                                                <tr nowrap>
                                                    <td class="dir">NW</td>
                                                    <td class="data">1.58</td>
                                                    <td class="data">4.28</td>
                                                    <td class="data">1.28</td>
                                                    <td class="data">0.76</td>
                                                    <td class="data">0.66</td>
                                                    <td class="data">0.69</td>
                                                    <td class="data">0.03</td>
                                                    <td class="data">9.28</td>
                                                </tr>
                                                <tr nowrap bgcolor="#DDDDDD">
                                                    <td class="dir">NNW</td>
                                                    <td class="data">1.51</td>
                                                    <td class="data">5.00</td>
                                                    <td class="data">1.32</td>
                                                    <td class="data">0.13</td>
                                                    <td class="data">0.23</td>
                                                    <td class="data">0.13</td>
                                                    <td class="data">0.07</td>
                                                    <td class="data">8.39</td>
                                                </tr>
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
    <form action="" id="selector">
        <input type="hidden" name="queryType" id="id-queryType">
        <input type="hidden" name="timeType" id="id-timeType">
        <input type="hidden" name="oldQueryTime" id="id-oldQueryTime">
        <input type="hidden" name="day" id="id-day">
        <input type="hidden" name="hour" id="id-hour">
    </form>

            </div>
        </div>
    </div>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse"><i class="icon-double-angle-up icon-only bigger-110"></i></a>
</div><!--[if !IE]> -->
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<script src="/public/statics/aceadmin/js/bootstrap.min.js"></script>
      <!--[if lte IE 8]>
<script src="/public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/public/statics/aceadmin/js/ace.min.js"></script>
<script src="/tpl/Public/js/base.js"></script>
<link href="https://cdn.bootcss.com/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="https://img.hcharts.cn/highcharts/highcharts.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/exporting.js"></script>
<script src="https://img.hcharts.cn/highcharts-plugins/highcharts-zh_CN.js"></script>
<script src="https://img.hcharts.cn/highcharts/highcharts.js"></script>
<script src="https://img.hcharts.cn/highcharts/highcharts-more.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/exporting.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/data.js"></script>
<script src="https://img.hcharts.cn/highcharts-plugins/highcharts-zh_CN.js"></script>

    <script>
        window.onload = function (ev) {
            changeQueryType()
            changeOldQueryType()
            changeTimeType()
            $("#id-date-picker-1").datepicker({
                changeMonth: false,
                showSecond: false,
                startDate: '2018/09/01',
                endDate: '<?php echo ($endDate); ?>',
                format: 'yyyy/mm/dd/',
            })
            $("#selector").submit(function () {
                $('#id-queryType').val($("#queryType").val())
                $('#id-oldQueryTime').val($("#oldQueryTime").val())
                $('#id-timeType').val($("#timeType").val())
                $('#id-hour').val($("#hour").val())
                $('#id-day').val($("#id-date-picker-1").val())
            })
        }
        function changeQueryType() {
            $(".queryType2").hide()
            $(".queryType1").hide()
            $(".oldQueryTime1").hide()
            $(".oldQueryTime2").hide()
            var queryType = $("#queryType").val()
            if (queryType == 1) {
                
                $(".queryType1").show()

            } else {
                $(".queryType2").show()
            }
        }
        function changeTimeType() {
            $(".timeType1").hide()
            $(".timeType2").hide()
            var timeType = $("#showType").val()
            if (timeType == 1) {
                $(".timeType1").show()

            } else {
                $(".timeType2").show()
            }
        }
        function changeOldQueryType() {
            $(".oldQueryTime1").hide()
            $(".oldQueryTime2").hide()
            var oldQueryTime = $("#oldQueryTime").val()

            if (oldQueryTime == 1) {
                $(".oldQueryTime1").show()
            } else {
                $(".oldQueryTime2").show()
            }
        }

        // geetest_show_verifyasd
        // function changeTimeType(el) {
            // window.location.href = "<?php echo U('Admin/Air/Index/timeType/');?>" + "/" + $('#timeType').val()
        // }
        // 使用数据功能模块进行数据解析
        // 使用数据功能模块进行数据解析
        var chart = Highcharts.chart('meigui', {
            data: {
                table: 'freq',
                startRow: 1,
                endRow: 17,
                endColumn: 7
            },
            chart: {
                polar: true,
                type: 'column'
            },
            title: {
                text: '空气监测玫瑰图'
            },
            subtitle: {
                text: ''
            },
            pane: {
                size: '100%'
            },
            legend: {
                align: 'right',
                verticalAlign: 'top',
                y: 100,
                layout: 'vertical'
            },
            xAxis: {
                tickmarkPlacement: 'on'
            },
            yAxis: {
                min: 0,
                endOnTick: false,
                showLastLabel: true,
                title: {
                    text: 'ug/m³'
                },
                labels: {
                    formatter: function () {
                        // return this.value + '%';
                        return this.value;
                    }
                },
                reversedStacks: false
            },
            tooltip: {
                valueSuffix: '%'
            },
            plotOptions: {
                series: {
                    stacking: 'normal',
                    shadow: false,
                    groupPadding: 0,
                    pointPlacement: 'on'
                }
            }
        });
        
        
        var chart1 = Highcharts.chart('qushitu', {
            chart: {
                type: 'spline'
            },
            title: {
                text: '空气监测趋势图'
            },
            subtitle: {
                text: '<?php echo ($chartName); ?>'
            },
            xAxis: {
                categories: <?php echo ($categories); ?>
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        // return this.value + '°';
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: <?php echo ($categoriesData); ?>
        });
      
    </script>

<script>
    $(function () {
        $('.b-has-child .active').parents('.b-has-child').eq(0).find('.b-nav-parent').click();
    })
</script>
</body>
</html>