<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
         中天钢铁环境空气质量监测平台
    </title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css" /><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css" /><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <link rel="stylesheet" href="/tpl/Public/css/base.css" />
    
    <style type="text/css">
        #sidebar .nav-list {
            overflow-y: auto;
        }
        
        .b-nav-li {
            padding: 5px 0;
        }
       
    </style>
    
</head>
<body>
<div class="navbar navbar-default" id="navbar">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left"><a href="#" class="navbar-brand">
            <small><i class="icon-leaf"></i> 中天钢铁环境空气质量监测平台</small>
        </a></div>
        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <span class="user-info"><small>欢迎光临,</small> <?php echo ($_SESSION['user']['username']); ?></span><i class="icon-caret-down"></i></a>
                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li class="divider"></li>
                        <li><a href="<?php echo U('Home/Index/logout');?>"><i class="icon-off"></i> 退出</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="main-container-inner"><a class="menu-toggler" id="menu-toggler" href="#"><span class="menu-text"></span></a>
        <div class="sidebar" id="sidebar">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span><span class="btn btn-info"></span><span class="btn btn-warning"></span><span class="btn btn-danger"></span>
                </div>
            </div><!-- #sidebar-shortcuts -->
            <ul class="nav nav-list">
                <?php if(is_array($nav_data)): foreach($nav_data as $key=>$v): if(empty($v['_data'])): ?><li class="b-nav-li"><a href="<?php echo U($v['mca']);?>"><i class="fa fa-<?php echo ($v['ico']); ?> icon-test"></i>
                            <span class="menu-text"><?php echo ($v['name']); ?></span></a></li>
                        <?php else: ?>
                        <li class="b-has-child">
                            <a href="#" class="dropdown-toggle b-nav-parent"><i class="fa fa-<?php echo ($v['ico']); ?> icon-test"></i>
                                <span class="menu-text"><?php echo ($v['name']); ?></span><b class="arrow icon-angle-down"></b></a>
                            <ul class="submenu">
                                <?php if(is_array($v['_data'])): foreach($v['_data'] as $key=>$n): ?><li class="b-nav-li <?php if( (MODULE_NAME. '/'.CONTROLLER_NAME.'/'.ACTION_NAME) == $n['mca']): ?>active<?php endif; ?>"><a href="<?php echo U($n['mca']);?>"><i class="icon-double-angle-right"></i> <?php echo ($n['name']); ?></a></li><?php endforeach; endif; ?>
                            </ul>
                        </li><?php endif; endforeach; endif; ?>
            </ul>
            
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {
                }
            </script>
        </div>
        <div class="main-content">
            <div class="page-content">
                
    <style>
        .topItem td {
            background-color: #c3c3c3 !important;
        }
        .sidebar-collapse{
            display: none;
        }
    </style>
    <div class="page-header"><h1><i class="fa fa-home"></i> <a href="<?php echo U('Admin/Index/Index');?>">首页</a> &gt;历史数据监控</h1>
    </div>
    <div class="col-xs-12">
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="clearfix">
                                <div class="pull-right tableTools-container"></div>
                            </div>
                            <div class="tabbable">
                                <form action="" id="selector">
                                    
                                    <div class="tab-content">
                                        <div id="home4" class="tab-pane active">
                                            
                                            <div class="table-header">
                                                <span>站点查询&nbsp;&nbsp;</span>
                                                <select>
                                                    <option selected>特钢大楼</option>
                                                </select>
                                                <span class="queryType2 oldQueryTime1">&nbsp;&nbsp;&nbsp;&nbsp;查询时间&nbsp;&nbsp;</span>
                                                <span class="">
                                                <input style="width: 120px;" name="day" value="<?php echo ($day); ?>" class=" date-picker" id="id-date-picker-1" type="text">
                                                </span>
                                                <span>&nbsp;&nbsp;数据区间&nbsp;&nbsp;</span>
                                                <select name="timeType" id="timeType">
                                                    <option value="1"
                                                    <?php if($timeType == 1): ?>selected<?php endif; ?>
                                                    >每5分钟</option>
                                                    <option value="2"
                                                    <?php if($timeType == 2): ?>selected<?php endif; ?>
                                                    >每1小时</option>
                                                </select>
                                                <button class="btn btn-sm  btn-yellow" onclick="$('#selector').submit()">&nbsp;&nbsp;查询</button>
                                            </div>
                                            <div class="table-header" style="background-color: white;border: 1px solid #ddd;color: black">
                                                
                                                
                                                显示数据： &nbsp;&nbsp;
                                                
                                                <input id="SO2" name="SO2" type="checkbox"
                                                <?php if($showSO2 == 1): ?>checked<?php endif; ?>
                                                onclick="showHideItem(8,'SO2')" value="1">
                                                <label for="SO2">二氧化硫SO₂</label> &nbsp;&nbsp;
                                                
                                                <input id="NO" name="NO" type="checkbox"
                                                <?php if($showNO == 1): ?>checked<?php endif; ?>
                                                value="1">
                                                <label for="NO">一氧化氮NO</label> &nbsp;&nbsp;
                                                
                                                <input id="NO2" name="NO2" type="checkbox"
                                                <?php if($showNO2 == 1): ?>checked<?php endif; ?>
                                                value="1">
                                                <label for="NO2">二氧化氮NO₂</label>
                                                
                                                &nbsp;&nbsp; <input id="NOX" name="NOX" type="checkbox"
                                                <?php if($showNOX == 1): ?>checked<?php endif; ?>
                                                value="1">
                                                <label for="NOX">氮氧化物NOX</label> &nbsp;&nbsp;
                                                <!---->
                                                <input id="CO" name="CO" type="checkbox"
                                                <?php if($showCO == 1): ?>checked<?php endif; ?>
                                                value="1">
                                                <label for="CO">一氧化碳CO(mg/m³)</label>
                                                
                                                &nbsp;&nbsp;
                                                
                                                <input id="O3" name="O3" type="checkbox"
                                                <?php if($showO3 == 1): ?>checked<?php endif; ?>
                                                value="1">
                                                <label for="O3">臭氧O₃</label>
                                                
                                                &nbsp;&nbsp;
                                                
                                                <input id="PM10" name="PM10" type="checkbox"
                                                <?php if($showPM10 == 1): ?>checked<?php endif; ?>
                                                value="1">
                                                <label for="PM10">PM10</label>
                                                
                                                <input id="PM25" name="PM25" type="checkbox"
                                                <?php if($showPM25 == 1): ?>checked<?php endif; ?>
                                                value="1">
                                                <label for="PM25">PM2.5</label>
                                            
                                            </div>
                                            <div>
                                                <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>时间</th>
                                                        <th class="item8">二氧化硫SO₂(ug/m³)</th>
                                                        
                                                        <th class="item2">一氧化氮NO(ug/m³)</th>
                                                        <th class="item3">二氧化氮NO₂(ug/m³)</th>
                                                        <th class="item4">氮氧化物NOX(ug/m³)</th>
                                                        <th class="item1">一氧化碳CO(mg/m³)</th>
                                                        
                                                        <th class="item5">臭氧O₃(ug/m³)</th>
                                                        
                                                        <th class="item6">PM10(ug/m³)</th>
                                                        <th class="item7">PM2.5(ug/m³)</th>
                                                    </tr>
                                                    </thead>
                                                    
                                                    <tbody>
                                                    <tr class="topItem">
                                                        <td>最大值</td>
                                                        <td class="item8"><?php echo getArrayMax($airData,'SO2')?></td>
                                                        <td class="item2"><?php echo getArrayMax($airData,'NO')?></td>
                                                        <td class="item3"><?php echo getArrayMax($airData,'NO2')?></td>
                                                        <td class="item4"><?php echo getArrayMax($airData,'NOX')?></td>
                                                        <td class="item1"></td>
                                                        <td class="item5"><?php echo getArrayMax($airData,'O3')?></td>
                                                        <td class="item6"><?php echo getArrayMax($airData,'PM10')?></td>
                                                        <td class="item7"><?php echo getArrayMax($airData,'PM25')?></td>
                                                    </tr>
                                                    <tr class="topItem">
                                                        <td>最小值</td>
                                                        <td class="item8"><?php echo getArrayLow($airData,'SO2')?></td>
                                                        <td class="item2"><?php echo getArrayLow($airData,'NO')?></td>
                                                        <td class="item3"><?php echo getArrayLow($airData,'NO2')?></td>
                                                        <td class="item4"><?php echo getArrayLow($airData,'NOX')?></td>
                                                        <td class="item1"></td>
                                                        <td class="item5"><?php echo getArrayLow($airData,'O3')?></td>
                                                        <td class="item6"><?php echo getArrayLow($airData,'PM10')?></td>
                                                        <td class="item7"><?php echo getArrayLow($airData,'PM25')?></td>
                                                    </tr>
                                                    <tr class="topItem">
                                                        <td>平均值</td>
                                                        <td class="item8"><?php echo getArrayAvg($airData,'SO2')?></td>
                                                        
                                                        <td class="item2"><?php echo getArrayAvg($airData,'NO')?></td>
                                                        <td class="item3"><?php echo getArrayAvg($airData,'NO2')?></td>
                                                        <td class="item4"><?php echo getArrayAvg($airData,'NOX')?></td>
                                                        <td class="item1"></td>
                                                        
                                                        <td class="item5"><?php echo getArrayAvg($airData,'O3')?></td>
                                                        <td class="item6"><?php echo getArrayAvg($airData,'PM10')?></td>
                                                        <td class="item7"><?php echo getArrayAvg($airData,'PM25')?></td>
                                                    </tr>
                                                    <tr class="topItem">
                                                        <td>超标个数</td>
                                                        <td class="item1"></td>
                                                        <td class="item2"></td>
                                                        <td class="item3"></td>
                                                        <td class="item4"></td>
                                                        <td class="item5"></td>
                                                        <td class="item6"></td>
                                                        <td class="item7"></td>
                                                        <td class="item8"></td>
                                                    </tr>
                                                    <tr class="topItem">
                                                        <td>超标率</td>
                                                        <td class="item1"></td>
                                                        <td class="item2"></td>
                                                        <td class="item3"></td>
                                                        <td class="item4"></td>
                                                        <td class="item5"></td>
                                                        <td class="item6"></td>
                                                        <td class="item7"></td>
                                                        <td class="item8"></td>
                                                    </tr>
                                                    <?php if(is_array($airData)): foreach($airData as $key=>$v): ?><tr>
                                                            <td><?php echo ($v['strTime']); ?></td>
                                                            <td class="item8"><?php echo ($v['SO2']); ?></td>
                                                            <td class="item2"><?php echo ($v['NO']); ?></td>
                                                            <td class="item3"><?php echo ($v['NO2']); ?></td>
                                                            <td class="item4"><?php echo ($v['NOX']); ?></td>
                                                            <td class="item1"></td>
                                                            <td class="item5"><?php echo ($v['O3']); ?></td>
                                                            <td class="item6"><?php echo ($v['PM10']); ?></td>
                                                            <td class="item7"><?php echo ($v['PM25']); ?></td>
                                                        </tr><?php endforeach; endif; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                            <div id="qushitu" style="min-width:400px;height:400px"></div>
                                        </div>
                                        
                                        <div id="profile4" class="tab-pane">
                                        
                                        
                                        </div>
                                    
                                    </div>
                                </form>
                            
                            </div>
                        
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>


            </div>
        </div>
    </div>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse"><i class="icon-double-angle-up icon-only bigger-110"></i></a>
</div><!--[if !IE]> -->
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
      <!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/tpl/Public/js/base.js"></script>
<link href="https://cdn.bootcss.com/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="https://img.hcharts.cn/highcharts/highcharts.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/exporting.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/series-label.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/oldie.js"></script>
<script src="https://img.hcharts.cn/highcharts-plugins/highcharts-zh_CN.js"></script>
<script src="https://img.hcharts.cn/highcharts/themes/grid-light.js"></script>


    <script>

        showHideItem(1, 'CO')
        showHideItem(2, 'NO')
        showHideItem(3, 'NO2')
        showHideItem(4, 'NOX')
        showHideItem(5, 'O3')
        showHideItem(6, 'PM10')
        showHideItem(7, 'PM25')
        showHideItem(8, 'SO2')
        window.onload = function (ev) {
            $("#id-date-picker-1").datepicker({
                changeMonth: false,
                showSecond: false,
                startDate: '2018-09-13',
                endDate: '<?php echo ($endDate); ?>',
                format: 'yyyy-mm-dd',
            })


            $("#selector").submit(function () {
                // $('#id-queryType').val($("#queryType").val())
                // $('#id-oldQueryTime').val($("#oldQueryTime").val())
                // $('#id-timeType').val($("#timeType").val())
                // $('#id-hour').val($("#hour").val())
                // $('#id-day').val($("#id-date-picker-1").val())
            })
        }

        function showHideItem(type, whitch) {
            console.dir(whitch)
            if ($('#' + whitch).is(":checked") == true) {
                // if($('#'+whitch).attr("checked")==true){
                console.dir(1)
                $('.item' + type).show()
            } else {
                console.dir(2)
                $('.item' + type).hide()
            }
        }

        function changeTimeType() {
            $(".timeType1").hide()
            $(".timeType2").hide()
            var timeType = $("#showType").val()
            if (timeType == 1) {
                $(".timeType1").show()

            } else {
                $(".timeType2").show()
            }
        }

        function changeOldQueryType() {
            $(".oldQueryTime1").hide()
            $("#id-date-picker-1").hide()
            var oldQueryTime = $("#oldQueryTime").val()
            if (oldQueryTime == 1) {
                $(".oldQueryTime1").show()
            } else {
                $("#id-date-picker-1").show()
            }
        }

        // geetest_show_verifyasd
        // function changeTimeType(el) {
        // window.location.href = "<?php echo U('Admin/Air/Index/timeType/');?>" + "/" + $('#timeType').val()
        // }
        // 使用数据功能模块进行数据解析
        // 使用数据功能模块进行数据解析


        var chart1 = Highcharts.chart('qushitu', {
            chart: {
                type: 'spline'
            },
            title: {
                text: '空气监测趋势图'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: <?php echo ($categories); ?>
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        // return this.value + '°';
                        return this.value;
                    }
                }
            },
            tooltip: {
                crosshairs: true,
                shared: true
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: <?php echo ($categoriesData); ?>
        });
    
    </script>

<script>
    $(function () {
        $('.b-has-child .active').parents('.b-has-child').eq(0).find('.b-nav-parent').click();
    })
</script>
</body>
</html>