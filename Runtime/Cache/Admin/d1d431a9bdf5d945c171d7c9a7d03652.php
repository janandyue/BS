<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
         中天钢铁环境空气质量监测平台
    </title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css" /><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css" /><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <link rel="stylesheet" href="/tpl/Public/css/base.css" />
    
    <style type="text/css">
        #sidebar .nav-list {
            overflow-y: auto;
        }
        
        .b-nav-li {
            padding: 5px 0;
        }
        .tab-content{
            /*padding: 6px 12px;*/
            padding: 6px 0;
            border: none;
        }
        .page-header{
            margin-bottom: 0;
            padding: 0 12px;
        }
        .page-header h1{
            font-size:  18px;
        }
        .btn-scroll-up{
            position: fixed;
            bottom: 120px;
        }
    </style>
    
</head>
<body>
<div class="navbar navbar-default" id="navbar">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left"><a href="#" class="navbar-brand">
            <small><i class="icon-leaf"></i> 中天钢铁环境空气质量监测平台</small>
        </a></div>
        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <span class="user-info"><small>欢迎光临,</small> <?php echo ($_SESSION['user']['username']); ?></span><i class="icon-caret-down"></i></a>
                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li class="divider"></li>
                        <li><a href="<?php echo U('Home/Index/logout');?>"><i class="icon-off"></i> 退出</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="main-container-inner"><a class="menu-toggler" id="menu-toggler" href="#"><span class="menu-text"></span></a>
        <div class="sidebar" id="sidebar">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span><span class="btn btn-info"></span><span class="btn btn-warning"></span><span class="btn btn-danger"></span>
                </div>
            </div><!-- #sidebar-shortcuts -->
            <ul class="nav nav-list">
                <?php if(is_array($nav_data)): foreach($nav_data as $key=>$v): if(empty($v['_data'])): ?><li class="b-nav-li"><a href="<?php echo U($v['mca']);?>"><i class="fa fa-<?php echo ($v['ico']); ?> icon-test"></i>
                            <span class="menu-text"><?php echo ($v['name']); ?></span></a></li>
                        <?php else: ?>
                        <li class="b-has-child">
                            <a href="#" class="dropdown-toggle b-nav-parent"><i class="fa fa-<?php echo ($v['ico']); ?> icon-test"></i>
                                <span class="menu-text"><?php echo ($v['name']); ?></span><b class="arrow icon-angle-down"></b></a>
                            <ul class="submenu">
                                <?php if(is_array($v['_data'])): foreach($v['_data'] as $key=>$n): ?><li class="b-nav-li <?php if( (MODULE_NAME. '/'.CONTROLLER_NAME.'/'.ACTION_NAME) == $n['mca']): ?>active<?php endif; ?>"><a href="<?php echo U($n['mca']);?>"><i class="icon-double-angle-right"></i> <?php echo ($n['name']); ?></a></li><?php endforeach; endif; ?>
                            </ul>
                        </li><?php endif; endforeach; endif; ?>
            </ul>
            
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {
                }
            </script>
        </div>
        <div class="main-content">
            <div class="page-content">
                
    <style>
        
        #home th {
            text-align: center;
            
        }
        
        #home td {
            text-align: center;
            
        }
        
        .topItem td {
            background-color: #c3c3c3 !important;
        }
        
        .sidebar-collapse {
            display: none;
        }
        
        path[fill=none].highcharts-label-box + text {
            display: none;
        }
        
        .fixed_headers thead tr {
            
            display: block;
            position: relative;
        }
        
        .fixed_headers tbody {
            display: block;
            overflow: auto;
            height: 580px;
        }
        
        .fixed_headers td:nth-child(1),
        .fixed_headers th:nth-child(1) {
            min-width: 160px;
        }
        
        .fixed_headers td,
        .fixed_headers th {
            width: 130px;
            
        }
        
        ::-webkit-scrollbar {
            display: none;
        }
        
        .sfixed {
            position: fixed;
            top: 0;
            left: 216px;
            z-index: 10;
            display: flex;
        }
        
        #dynamic-table {
            table-layout: fixed;
            min-width: 100%;
        }
        
        #dynamic-table tr th {
            flex: 1;
        }
        
        #dynamic-table tr th, #dynamic-table tr td {
            /*width: 10%!important;*/
            /*min-width: 86px!important;*/
            /*width: 129px;*/
        }
    </style>
    <div class="page-header"><h1><i class="fa fa-home"></i> <a href="<?php echo U('Admin/Index/Index');?>">首页</a> &gt;实时数据监控</h1>
    </div>
    <div class="col-xs-12">
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="clearfix">
                                <div class="pull-right tableTools-container"></div>
                            </div>
                            <div class="tabbable">
                                
                                <div class="tab-content">
                                    <div id="home4" class="tab-pane active">
                                        
                                        
                                        <div>
                                            
                                            <ul class="nav nav-tabs padding-16">
                                                <li class="active" onclick="$('#hideBtn').hide()">
                                                    <a data-toggle="tab" href="#home">
                                                        站点列表
                                                    </a>
                                                </li>
                                                <li onclick="$('#hideBtn').hide()">
                                                    <a data-toggle="tab" href="#add">
                                                        添加站点
                                                    </a>
                                                </li>
                                                <li id="hideBtn" style="display: none">
                                                    <a data-toggle="tab" id="hid" href="#edit">
                                                        修改站点信息
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        
                                        <div class="tab-content">
                                            <div id="home" class="tab-pane in active">
                                                
                                                
                                                <table id="dynamic-table" class="table table-striped table-bordered table-hover ">
                                                    <thead>
                                                    <tr class="scroll">
                                                        <th class="item8">站点编号</th>
                                                        <th class="item2">站点名称</th>
                                                        <th class="item3">添加时间</th>
                                                        <th class="item4">状态</th>
                                                        <th class="item4">操作</th>
                                                    </tr>
                                                    </thead>
                                                    
                                                    <tbody>
                                                    <?php if(is_array($siteList)): foreach($siteList as $key=>$v): ?><tr>
                                                            <td>#<?php echo ($v['id']); ?></td>
                                                            <td><?php echo ($v['site_name']); ?></td>
                                                            <td><?php echo ($v['site_addtime']); ?></td>
                                                            <td><?php echo ($v['status']); ?></td>
                                                            <td>
                                                                <a href="javascript:;" onclick="edit(<?php echo ($v['id']); ?>,'<?php echo ($v['site_name']); ?>')">修改</a>
                                                                |
                                                                <a href="javascript:;" onclick="changeStatus(<?php echo ($v['id']); ?>,'<?php echo ($v['site_name']); ?>',<?php echo ($v['is_use']); ?>)"><?php echo ($v['btn']); ?></a>
                                                                |
                                                                <a href="javascript:;" onclick="del(<?php echo ($v['id']); ?>,'<?php echo ($v['site_name']); ?>')">删除站点</a>
                                                            </td>
                                                        </tr><?php endforeach; endif; ?>
                                                    </tbody>
                                                </table>
                                            
                                            </div>
                                            
                                            <div id="add" class="tab-pane">
                                                <div class="modal-body">
                                                    <form action="<?php echo U('Admin/Site/add');?>" id="form_add" class="form-inline" method="post">
                                                        <input type="hidden" name="id">
                                                        <table class="table table-striped table-bordered table-hover table-condensed">
                                                            <tr>
                                                                <th width="12%">站点名称：</th>
                                                                <td>
                                                                    <input class="input-medium" type="text" name="site_name">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>状态</th>
                                                                <td>
                                                                    <span class="inputword">启用</span>
                                                                    <input class="xb-icheck" type="radio" name="is_use" value="1" checked> &emsp;
                                                                    <span class="inputword">禁用</span>
                                                                    <input class="xb-icheck" type="radio" name="is_use" value="0">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th></th>
                                                                <td>
                                                                    <input class="btn btn-success" type="button" onclick="addSite()" value="添加站点">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                            <div id="edit" class="tab-pane">
                                                <div class="tab-pane">
                                                    <div class="modal-body">
                                                        <form id="" action="<?php echo U('Admin/Site/edit');?>" class="form-inline" method="post">
                                                            <input type="hidden" name="id">
                                                            <table class="table table-striped table-bordered table-hover table-condensed">
                                                                <tr>
                                                                    <th width="12%">站点名称：</th>
                                                                    <td>
                                                                        <input class="input-medium" type="text" name="site_name">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th>状态</th>
                                                                    <td>
                                                                        <span class="inputword">启用</span>
                                                                        <input class="xb-icheck" type="radio" name="is_use" value="1"> &emsp;
                                                                        <span class="inputword">禁用</span>
                                                                        <input class="xb-icheck" type="radio" name="is_use" value="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th></th>
                                                                    <td>
                                                                        <input class="btn btn-success" type="submit" value="修改站点">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </form>
                                                    </div>
                                                </div>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div id="profile4" class="tab-pane">
                                    
                                    
                                    </div>
                                
                                </div>
                            
                            </div>
                        
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>


            </div>
        </div>
    </div>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse"><i class="icon-double-angle-up icon-only bigger-110"></i></a>
</div><!--[if !IE]> -->
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
      <!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/tpl/Public/js/base.js"></script>
<link href="https://cdn.bootcss.com/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="https://img.hcharts.cn/highcharts/highcharts.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/exporting.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/series-label.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/oldie.js"></script>
<script src="https://img.hcharts.cn/highcharts-plugins/highcharts-zh_CN.js"></script>
<script src="https://img.hcharts.cn/highcharts/themes/grid-light.js"></script>


    <script>
        function edit(id) {
            $("#hideBtn").show()
            $("#hid").trigger("click");
            $.ajax({
                url: "<?php echo U('Admin/Site/SiteInfo');?>",
                type: 'GET',     // 请求类型，常用的有 GET 和 POST
                data: {id: id},
                dataType: 'json',
                success: function (dd) { // 接口调用成功回调函数
                    $("#edit").find("input[name=site_name]").val(dd.site_name)
                    $("#edit").find("input[name=id]").val(dd.id)
                    $("#edit").find("input[name=is_use][value=" + dd.is_use + "]").attr("checked", true);
                }
            })
        }

        function changeStatus(id, name, status) {
            var statusName = status == 1 ? "禁用" : "启用"
            var flag = confirm('是否' + statusName + '站点「' + name + '」')
            if (flag) {
                window.location.href = "<?php echo U('Admin/Site/change');?>?id=" + id

            } else {

            }

        }

        function del(id, name) {
            var flag = confirm('是否删除站点「' + name + '」')
            if (flag == true) {
                window.location.href = "<?php echo U('Admin/Site/del');?>?id=" + id
            } else {

            }
        }

        function addSite() {
            var siteNumber = 'Site_'+Math.random().toString(36).substr(8);;
            var siteName = $('#form_add').find('input[name=site_name]');
            $.ajax({
                url: "AirCollectionApi/api/Site",
                type: 'POST',
                data: {
                    siteNumber: siteNumber,
                    siteName: siteName,
                    type: 1
                },
                dataType:"json",
                success:function (dd) { // 接口调用成功回调函数
                    if(dd=="success"){
                        alert("添加站点成功")
                        window.location.reload()
                    }else{
                        alert("添加站点失败，请联系管理员")

                    }
                }
            })

        }

     

    </script>

<script>
    $(function () {
        $('.b-has-child .active').parents('.b-has-child').eq(0).find('.b-nav-parent').click();
    })
</script>
</body>
</html>