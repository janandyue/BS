<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>
         中天钢铁环境空气质量监测平台
    </title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="/public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/public/statics/aceadmin/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/public/statics/aceadmin/css/font-awesome-ie7.min.css" /><![endif]-->
    <link rel="stylesheet" href="/public/statics/aceadmin/css/ace.min.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/public/statics/aceadmin/css/ace-ie.min.css" /><![endif]--><!--[if lt IE 9]>
    <script src="/public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <link rel="stylesheet" href="/tpl/Public/css/base.css" />
    
    <style type="text/css">
        #sidebar .nav-list {
            overflow-y: auto;
        }
        
        .b-nav-li {
            padding: 5px 0;
        }
    </style>
    
        <link rel="stylesheet" href="/Public/statics/iCheck-1.0.2/skins/all.css">

</head>
<body>
<div class="navbar navbar-default" id="navbar">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left"><a href="#" class="navbar-brand">
            <small><i class="icon-leaf"></i> 中天钢铁环境空气质量监测平台</small>
        </a></div>
        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle"><img class="nav-user-photo" src="/public/statics/aceadmin/avatars/user.jpg" alt="Jason's Photo" />
                        <span class="user-info"><small>欢迎光临,</small> <?php echo ($_SESSION['user']['username']); ?></span><i class="icon-caret-down"></i></a>
                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li class="divider"></li>
                        <li><a href="<?php echo U('Home/Index/logout');?>"><i class="icon-off"></i> 退出</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="main-container" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="main-container-inner"><a class="menu-toggler" id="menu-toggler" href="#"><span class="menu-text"></span></a>
        <div class="sidebar" id="sidebar">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'fixed')
                } catch (e) {
                }
            </script>
            <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                    <button class="btn btn-success"><i class="icon-signal"></i></button>
                    <button class="btn btn-info"><i class="icon-pencil"></i></button>
                    <button class="btn btn-warning"><i class="icon-group"></i></button>
                    <button class="btn btn-danger"><i class="icon-cogs"></i></button>
                </div>
                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span><span class="btn btn-info"></span><span class="btn btn-warning"></span><span class="btn btn-danger"></span>
                </div>
            </div><!-- #sidebar-shortcuts -->
            <ul class="nav nav-list">
                <?php if(is_array($nav_data)): foreach($nav_data as $key=>$v): if(empty($v['_data'])): ?><li class="b-nav-li"><a href="<?php echo U($v['mca']);?>"><i class="fa fa-<?php echo ($v['ico']); ?> icon-test"></i>
                            <span class="menu-text"><?php echo ($v['name']); ?></span></a></li>
                        <?php else: ?>
                        <li class="b-has-child">
                            <a href="#" class="dropdown-toggle b-nav-parent"><i class="fa fa-<?php echo ($v['ico']); ?> icon-test"></i>
                                <span class="menu-text"><?php echo ($v['name']); ?></span><b class="arrow icon-angle-down"></b></a>
                            <ul class="submenu">
                                <?php if(is_array($v['_data'])): foreach($v['_data'] as $key=>$n): ?><li class="b-nav-li <?php if( (MODULE_NAME. '/'.CONTROLLER_NAME.'/'.ACTION_NAME) == $n['mca']): ?>active<?php endif; ?>"><a href="<?php echo U($n['mca']);?>"><i class="icon-double-angle-right"></i> <?php echo ($n['name']); ?></a></li><?php endforeach; endif; ?>
                            </ul>
                        </li><?php endif; endforeach; endif; ?>
            </ul>
            <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
            </div>
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {
                }
            </script>
        </div>
        <div class="main-content">
            <div class="page-content">
                
    <div class="page-header"><h1><i class="fa fa-home"></i> 首页 &gt; 权限管理 &gt; 添加管理员</h1></div>
    <div class="col-xs-12">
        <div class="tabbable">
            <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab">
                <li><a href="<?php echo U('Admin/Rule/admin_user_list');?>">管理员列表</a></li>
                <li class="active"><a href="<?php echo U('Admin/Rule/add_admin');?>">添加管理员</a></li>
            </ul>
            <div class="tab-content">
                <form class="form-inline" method="post">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <tr>
                            <th>管理组</th>
                            <td>
                                <?php if(is_array($data)): foreach($data as $key=>$v): echo ($v['title']); ?>
                                    <input class="xb-icheck" type="checkbox" name="group_ids[]" value="<?php echo ($v['id']); ?>"> &emsp;<?php endforeach; endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>姓名</th>
                            <td><input class="input-medium" type="text" name="username"></td>
                        </tr>
                        <tr>
                            <th>手机号</th>
                            <td><input class="input-medium" type="text" name="phone"></td>
                        </tr>
                        <tr>
                            <th>邮箱</th>
                            <td><input class="input-medium" type="text" name="email"></td>
                        </tr>
                        <tr>
                            <th>初始密码</th>
                            <td><input class="input-medium" type="text" name="password"></td>
                        </tr>
                        <tr>
                            <th>状态</th>
                            <td><span class="inputword">允许登录</span>
                                <input class="xb-icheck" type="radio" name="status" value="1" checked="checked"> &emsp;
                                <span class="inputword">禁止登录</span>
                                <input class="xb-icheck" type="radio" name="status" value="0"></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td><input class="btn btn-success" type="submit" value="添加"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>

            </div>
        </div>
    </div>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse"><i class="icon-double-angle-up icon-only bigger-110"></i></a>
</div><!--[if !IE]> -->
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<script src="/public/statics/aceadmin/js/bootstrap.min.js"></script>
      <!--[if lte IE 8]>
<script src="/public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/public/statics/aceadmin/js/ace.min.js"></script>
<script src="/tpl/Public/js/base.js"></script>
<link href="https://cdn.bootcss.com/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="https://img.hcharts.cn/highcharts/highcharts.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/exporting.js"></script>
<script src="https://img.hcharts.cn/highcharts-plugins/highcharts-zh_CN.js"></script>
<script src="https://img.hcharts.cn/highcharts/highcharts.js"></script>
<script src="https://img.hcharts.cn/highcharts/highcharts-more.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/exporting.js"></script>
<script src="https://img.hcharts.cn/highcharts/modules/data.js"></script>
<script src="https://img.hcharts.cn/highcharts-plugins/highcharts-zh_CN.js"></script>

    <script src="/Public/statics/iCheck-1.0.2/icheck.min.js"></script>
<script>
$(document).ready(function(){
    $('.xb-icheck').iCheck({
        checkboxClass: "icheckbox_minimal-blue",
        radioClass: "iradio_minimal-blue",
        increaseArea: "20%"
    });
});
</script>

<script>
    $(function () {
        $('.b-has-child .active').parents('.b-has-child').eq(0).find('.b-nav-parent').click();
    })
</script>
</body>
</html>