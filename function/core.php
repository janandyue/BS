<?php
/**
 * Created by PhpStorm.
 * User: jan
 * Date: 2018/8/20
 * Time: 12:42 PM
 */
ini_set('date.timezone','Asia/Shanghai');
session_start();

require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require $_SERVER['DOCUMENT_ROOT'] . '/function/curd.php';
require $_SERVER['DOCUMENT_ROOT'] . '/function/SITE_SETTING.php';
/*
 * 返回json字符串
 *
 */
function returnAjax($status = false, $data = array(), $message = "") {
    $data['status'] = $status;
    $data['data'] = $data;
    $data['message'] = $message;
    echo json_encode($data);
    exit;
}

/*
 * 写入登陆状态
 *
 */
function writeLoginSession($userInfo) {
    if (!empty($userInfo)) {
        $_SESSION['userId'] = $userInfo['userId'];
        $_SESSION['username'] = $userInfo['username'];
        return true;
    }
    else {
        return false;
    }

}

/*
 * loginOut
 *
 */
function loginOut() {
    unset($_SESSION['userId']);
    unset($_SESSION['username']);

}

/*
 * 检查登陆状态
 * 返回UserId 或者false
 *
 */
function checkLogin() {
    if (isset($_SESSION['userId']) && $_SESSION['userId'] > 0) {
        return $_SESSION['userId'];
    }
    else {
        return false;
    }
}

function headerCheck() {
    if (checkLogin() == false) {
        JUMP("../index.php");
    }
}

function JUMP($fileName) {
    header('Location:' . $fileName);
}

function getNowFile() {
    $PHP_SELF = $_SERVER['PHP_SELF'];
    $fiexplode = explode('/admin/', $PHP_SELF);
    return $fiexplode[1];
}

function getLeftMenu() {
    $nowFile = getNowFile();
    $menu = array();
    $menu[] = array(
        'name' => "雨量监测",
        'son' => array(
            array(
                'name'=>'总览',
                'link'=>'ev.php?type=a',
            ),
            array(
                'name'=>'乌嘴桥',
                'link'=>'ev.php?type=a',
            ),
            array(
                'name'=>'河马洞',
                'link'=>'ev.php?type=d',
            ),
            array(
                'name'=>'马庄',
                'link'=>'ev.php?type=c',
            ),
            array(
                'name'=>'福利桥',
                'link'=>'ev.php?type=b',
            ),

        ),
    );
    $menu[] = array(
        'name' => "空气监测",
        'son' => array(
            array(
                'name'=>'总览',
                'link'=>'ev.php?type=a',
            ),
            array(
                'name'=>'乌嘴桥',
                'link'=>'air.php?type=a',
            ),
            array(
                'name'=>'河马洞',
                'link'=>'air.php?type=b',
            ),
            array(
                'name'=>'马庄',
                'link'=>'air.php?type=c',
            ),
            array(
                'name'=>'福利桥',
                'link'=>'air.php?type=d',
            ),

        ),
    );
    return $menu;

}

function p($value = "Test") {
    print_r($value);
    exit;
}

function valueFamat($fm,$val){
    if($val==0){
        return "";
    }else{
        if($val>1000){
            return sprintf($fm, $val/1000);
        }else{
            return sprintf($fm, $val);

        }
    }

}
